import array, struct, math, os, time, sys, itertools, getopt
import zipfile, zlib
import mathutils, bpy, bpy_extras.io_utils, bpy_extras.node_shader_utils

################################################################################
# equal tests
################################################################################
_eps_ = 0.0001

def is_zero(a, eps = _eps_):
  return abs(a) < eps

def is_zero2(a, eps = _eps_):
  return is_zero(a[0], eps) and is_zero(a[1], eps)

def is_zero3(a, eps = _eps_):
  return is_zero(a[0], eps) and is_zero(a[1], eps) and is_zero(a[2], eps)


def is_one(a, eps = _eps_):
  return abs(a - 1) < eps

def is_one2(a, eps = _eps_):
  return is_one(a[0], eps) and is_one(a[1], eps)

def is_one3(a, eps = _eps_):
  return is_one(a[0], eps) and is_one(a[1], eps) and is_one(a[2], eps)


def is_equal(a, b, eps = _eps_):
    return abs(a - b) <= eps

def is_equal2(a, b, eps = _eps_):
    return is_equal(a.x, b.x, eps) and is_equal(a.y, b.y, eps) #return a == b

def is_equal3(a, b, eps = _eps_):
    return is_equal(a.x, b.x, eps) and is_equal(a.y, b.y, eps) and is_equal(a.z, b.z, eps) #return a == b

################################################################################

def floats_to_vec3s(list):
    vecs = []
    n = len(list) // 3
    for i in range(0, n):
        t = (list[(3*i)+0], list[(3*i)+1], list[(3*i)+2])
        vecs.append(mathutils.Vector(t))
    return vecs

################################################################################

def index_type_str(count):
    if count < 256:
        return 'byte'
    if count < 65536:
        return 'short'
    return 'int'

def index_type_struct(count):
    if count < 256:
        return 'B'
    if count < 65536:
        return 'H'
    return 'I'

################################################################################

def find_armature_in_modifiers(armature, modifiers):
    for mod in modifiers:
        if mod.type == 'ARMATURE' and armature == mod.object:
            return mod
    return None

def obj_name(obj):
    if obj.type != 'ARMATURE' and (obj.parent == None or obj.parent_type != 'ARMATURE') and obj.data != None and obj.name != obj.data.name:
        return obj.name + "@" + obj.data.name
    else:
        return obj.name


def mesh_name(obj):
    return obj.data.name
    # if obj.parent_type != 'ARMATURE':# and obj.parent_type != 'BONE':
    #     return obj.data.name
    # return obj.name

def is_armature_mesh(obj):
    if obj.parent == None or obj.parent.type != 'ARMATURE' or obj.parent.parent != None:
        return False
    has_armature_parent = obj.parent_type == 'ARMATURE'
    has_armature_modifier = find_armature_in_modifiers(obj.parent, obj.modifiers) != None
    return has_armature_parent or has_armature_modifier

def is_bone_mesh(obj):
    return obj.parent_type == 'BONE' and len(obj.parent_bone) > 0 \
       and obj.parent != None and obj.parent.type == 'ARMATURE' \
       and obj.parent.parent == None

def is_empty_mesh(obj):
    return obj.parent_type == 'OBJECT'  \
       and obj.parent != None and obj.parent.type == 'EMPTY' \
       and obj.parent.parent == None

def is_valid_mesh(obj, withAnim=False, rooted=False):
    if (obj.type != 'MESH') or ( len(obj.data.vertices.values()) == 0 ):
        return False
    if (obj.parent != None and not (is_armature_mesh(obj) or is_bone_mesh(obj) or is_empty_mesh(obj))):
        return False
    if withAnim and not is_animated(obj):
        return False
    if rooted and obj.parent != None:
        return False
    return True

def is_animated(obj):
    anim = obj.animation_data and (obj.animation_data.action or len(obj.animation_data.nla_tracks) > 0)
    if hasattr(obj, 'parent_type'):
      anim = anim or obj.parent_type == 'BONE' or obj.parent_type == 'ARMATURE' or obj.parent_type == 'EMPTY'
    return anim

def findMeshes(objects, withAnim=False, rooted=False):
    meshes = []
    for obj in objects:
        if obj.hide_viewport:
            continue

        if not is_valid_mesh(obj, withAnim, rooted):
            continue

        # prevent exporting twice
        id = mesh_name(obj)
        skip = False
        for (i, mesh) in enumerate(meshes):
            if id == mesh_name(mesh):
                skip = True
                # if object has (more) vertex_groups give that one priority for skin export
                if len(mesh.vertex_groups) > len(mesh.vertex_groups):
                    meshes[i] = mesh
                break
        if skip:
            continue

        meshes.append(obj)
    return meshes

def findModels(objects, withAnim=False):
    result = []
    for obj in objects:
        if obj.hide_viewport:
            continue
        if obj.parent != None: # only rooted objects
            continue
        if obj.type != 'ARMATURE' and obj.type != 'EMPTY':
            continue
        if withAnim and not is_animated(obj):
            continue
        result.append(obj)
    return result

def findRootObjects(objects, withAnim=False):
    result = []
    for obj in objects:
        if obj.hide_viewport:
            continue
        if obj.parent != None: # only rooted objects
            continue
        if obj.type != 'ARMATURE' and obj.type != 'MESH' and obj.type != 'EMPTY':
            continue
        if withAnim and not is_animated(obj):
            continue
        result.append(obj)
    return result

def jsonElem(file_json, next, nl = True):
    if next:
        file_json.write(',')
    if nl:
        file_json.write('\n')


################################################################################
# Material Textures
################################################################################

def find_material_textures(mat):
    textures = []
    if mat and mat.node_tree:
        for node in mat.node_tree.nodes:
            if node.bl_idname == 'ShaderNodeTexImage':
                if node.image:
                    textures.append(node.image.filepath)
    return textures

################################################################################
# Triangulate
################################################################################

def poly_triangulate(points):
    map = []
    triangles = mathutils.geometry.tessellate_polygon((points,))
    for triangle in triangles:
                map.append(triangle[0])
                map.append(triangle[2])
                map.append(triangle[1])
    return map

def poly_triangulate_concave(points):
    n = len(points)
    if n < 3:
        return []

    indices = []
    for i in range(1, len(points) - 1):
        indices.append(0)
        indices.append(i)
        indices.append(i+1)
    return indices

################################################################################
# Group
################################################################################

class Group:
    def __init__(self, name="", count=0, offset=0, type='elements'):
        self.name = name
        self.count = count
        self.offset = offset
        self.type = type

    def json_str(self):
        json = '{ "name": "%s", "count": %d' % (self.name, self.count)
        json += ', "offset": %d' % self.offset if self.offset > 0 else ''
        json += ', "type": "%s"' % self.type if self.type != 'elements' else ''
        json += ' }'
        return json

################################################################################
# Attribute
################################################################################

class Attribute:
    def __init__(self, name="", type="float", size=0, frames=1):
        self.name = name
        self.size = size
        self.type = type
        self.frames = frames

    def json_str(self):
        json = '{ "name": "%s", "type": "%s", "size": %d' % (self.name, self.type, self.size)
        json += ', "frames": %d }' % self.frames if self.frames > 1 else ' }'
        return json

    def create_array(self, count=1):
        if self.type == 'float':
            return array.array('f', [0.0] * self.size * self.frames * count)
        if self.type == 'int':
            return array.array('I', [0] * self.size * self.frames * count)
        if self.type == 'short':
            return array.array('H', [0] * self.size * self.frames * count)
        if self.type == 'byte':
            return array.array('B', [0] * self.size * self.frames * count)

################################################################################
# Samplers
################################################################################

class Sampler:
    @staticmethod
    def from_bpy_bone(frames, obj, bone, relative=True):
        return [ SamplerTransform3D(frames, obj, bone, relative) ]

    @staticmethod
    def from_bpy_object(frames, obj, relative=True):
        samplers = Sampler.from_bpy_object_morphs(frames, obj)
        samplers.append(SamplerTransform3D(frames, obj, None, relative))
        return samplers

    @staticmethod
    def from_bpy_object_morphs(frames, obj):
        if obj.type != 'MESH':
            return []
        if not obj.data.shape_keys:
            return []
        if not is_animated(obj.data.shape_keys):
            return []

        samplers = []

        prefix = obj.name + "."
        ref = obj.data.shape_keys.reference_key
        for block in obj.data.shape_keys.key_blocks:
            if block.name == ref.name:
                continue
            samplers.append(SamplerMorphTarget(frames, block, prefix))

        return samplers


class SamplerMorphTarget:
    _s_float = struct.Struct('<f')

    def __init__(self, frames, block, prefix=""):
        self.frames = frames
        self.block = block
        self.prefix = prefix
        self.valid = False
        self.data = array.array('f', [0.0] * frames)

    def sample(self, frame):
        s_float = SamplerMorphTarget._s_float
        self.valid = self.valid or not is_zero(self.block.value)
        s_float.pack_into(self.data, frame*s_float.size, self.block.value)

    def to_channel(self):
        if not self.valid:
            return None
        attr = Attribute('morph', 'float', 1, self.frames)
        return Channel(self.prefix + self.block.name, [ attr ], [ self.data ])


class SamplerTransform3D:
    _s_vec3 = struct.Struct('<fff')

    @staticmethod
    def _pose_tm(obj, bone):
        m = mathutils.Matrix.Identity(4)
        if bone != None:
            m = bone.bone.matrix_local
            if bone.parent != None:
                m = (bone.parent.bone.matrix_local).inverted() @ m
        else:
            m = obj.matrix_local
            if obj.parent != None and obj.parent.type == 'ARMATURE' and obj.parent_type == 'BONE':
                p_bone = obj.parent.pose.bones[obj.parent_bone]
                m = (obj.parent.matrix_world @ p_bone.bone.matrix_local).inverted() @ obj.matrix_world
        m = m.inverted()
        return m

    @staticmethod
    def _frame_tm(obj, bone):
        m = mathutils.Matrix.Identity(4)
        if bone != None:
            m = bone.matrix
            if bone.parent != None:
                m = (bone.parent.matrix).inverted() @ m
        else:
            m = obj.matrix_local
            if obj.parent != None and obj.parent.type == 'ARMATURE' and obj.parent_type == 'BONE':
                p_bone = obj.parent.pose.bones[obj.parent_bone]
                m = (obj.parent.matrix_world @ p_bone.matrix).inverted() @ obj.matrix_world
        return m

    def __init__(self, frames, obj, bone=None, relative=True):
        self.frames = frames
        self.obj = obj
        self.bone = bone
        self.relative = relative

        self.use_T = False
        self.use_R = False
        self.use_S = False
        self.data_T = array.array('f', [0.0] * 3 * self.frames)
        self.data_R = array.array('f', [0.0] * 3 * self.frames)
        self.data_S = array.array('f', [0.0] * 3 * self.frames)
        self.pose_TM = SamplerTransform3D._pose_tm(self.obj, self.bone)

    def sample(self, frame):
        s_vec3 = SamplerTransform3D._s_vec3

        matrix = SamplerTransform3D._frame_tm(self.obj, self.bone)
        if self.relative:
            matrix = self.pose_TM @ matrix
        translation, rotation, scale = matrix.decompose()

        offset = frame*s_vec3.size
        s_vec3.pack_into(self.data_T, offset, *translation)
        s_vec3.pack_into(self.data_R, offset, *rotation[1:4])
        s_vec3.pack_into(self.data_S, offset, *scale)

        self.use_T = self.use_T or not is_zero3(translation)
        self.use_R = self.use_R or not (is_one(rotation[0]) and is_zero(rotation[1]) and is_zero(rotation[2]) and is_zero(rotation[3]))
        self.use_S = self.use_S or not is_one3(scale)

    def to_channel(self):
        if not (self.use_T or self.use_R or self.use_S):
            return None

        name = self.obj.name + '.' + self.bone.name if self.bone != None else self.obj.name
        attrs = []
        data = []
        if self.use_T:
            attrs.append(Attribute("translate", "float", 3, self.frames))
            data.append(self.data_T)
        if self.use_R:
            attrs.append(Attribute("rotate", "float", 3, self.frames))
            data.append(self.data_R)
        if self.use_S:
            attrs.append(Attribute("scale", "float", 3, self.frames))
            data.append(self.data_S)
        return Channel(name, attrs, data)

################################################################################
# Channel
################################################################################

class Channel:
    def __init__(self, name="", attributes=[], data=[], begin=0.0, end=1.0):
        self.name = name
        self.begin = begin
        self.end = end
        self.attributes = attributes
        self.data = data

    def json_str(self, prefix='\t\t'):
        json = prefix + '{\n'
        json += prefix + '\t"name": "%s",' % (self.name)
        if self.begin > 0.0:
            json += '"from": %f\n' % self.begin
        if self.end < 1.0:
            json += '"to": %f\n' % self.end
        json += '\n'
        json += prefix + '\t"attributes": [\n'
        n = len(self.attributes)
        for (i, attr) in enumerate(self.attributes):
            json += prefix + '\t\t' + attr.json_str()
            json += ',\n' if i+1 < n else '\n'
        json += prefix + '\t]\n'
        json += prefix + '}'
        return json

    def data_iter(self):
        for d in self.data:
            yield d

################################################################################
# Action
################################################################################

class Action:
    @staticmethod
    def from_nla_tracks(scene, obj):
        if not obj.animation_data:
            return []

        count = (scene.frame_end - scene.frame_start) #actually it is count-1, but that's what we want
        actions = []
        for track in obj.animation_data.nla_tracks:
            for i, strip in enumerate(track.strips):
                if strip.frame_start < scene.frame_start or strip.frame_end > scene.frame_end:
                    continue
                name = track.name
                if len(track.strips) > 1:
                    name = '%s[%d]' % (name, i)
                first = (strip.frame_start - scene.frame_start) / count
                last = (strip.frame_end - scene.frame_start) / count
                loop = ("loop" in strip.name) or ("cycle" in strip.name)
                actions.append(Action(name, first, last, loop))
        return actions

    def __init__(self, name="", begin=0.0, end=1.0, loop=False):
        self.name = name
        self.begin = begin
        self.end = end
        self.loop = loop

    def json_str(self):
        json = '{ "name": "%s", "from": %f, "to": %f' % (self.name, self.begin, self.end)
        json += ', "loop": true }' if self.loop else ' }'
        return json

################################################################################
# Animation
################################################################################

class Animation:
    @staticmethod
    def create_channels(scene, samplers):
        frame_current = scene.frame_current
        for (i, frame) in enumerate(range(scene.frame_start, scene.frame_end + 1)):
            scene.frame_set(frame)
            for sampler in samplers:
                sampler.sample(i)
        scene.frame_set(frame_current)

        channels = []
        for sampler in samplers:
            channel = sampler.to_channel()
            if channel:
                channels.append(channel)

        return channels


    @staticmethod
    def from_bpy_scene(scene):
        frames = (scene.frame_end - scene.frame_start) + 1
        duration = frames / scene.render.fps

        samplers = []
        for obj in findRootObjects(scene.objects, True):
            samplers.extend(Sampler.from_bpy_object(frames, obj))

        channels = Animation.create_channels(scene, samplers)
        return Animation(scene.name, duration, channels)

    @staticmethod
    def for_bpy_object(scene, obj, nla_actions=True):
        frames = (scene.frame_end - scene.frame_start) + 1
        duration = frames / scene.render.fps

        samplers = []
        actions = []
        if obj.type == 'EMPTY':
            for child in obj.children:
                if child.hide_viewport or not is_empty_mesh(child):
                    continue
                samplers.extend(Sampler.from_bpy_object(frames, child))
        elif obj.type == 'ARMATURE':
            if nla_actions:
                actions.extend(Action.from_nla_tracks(scene, obj))
            for bone in obj.data.bones:
                bone = obj.pose.bones[bone.name]
                samplers.extend(Sampler.from_bpy_bone(frames, obj, bone))
            for child in obj.children:
                if child.hide_viewport or not (is_bone_mesh(child) or is_armature_mesh(child)):
                    continue
                samplers.extend(Sampler.from_bpy_object(frames, child))

        channels = Animation.create_channels(scene, samplers)
        return Animation(obj.name, duration, channels, actions)

    def __init__(self, name="", duration=1.0, channels=[], actions=[]):
        self.name = name
        self.duration = duration
        self.channels = channels
        self.actions = actions

    def json_str(self):
        json = '{\n'
        json += '\t"duration": %f,\n' %  (self.duration)
        # actions
        n = len(self.actions)
        if n > 0:
            json += '\t"actions": [\n'
            for (i, action) in enumerate(self.actions):
                json += '\t\t' + action.json_str()
                json += ',\n' if i+1 < n else '\n'
            json += '\t],\n'
        # channels
        n = len(self.channels)
        json += '\t"channels": [\n'
        for (i, channel) in enumerate(self.channels):
            json += channel.json_str()
            json += ',\n' if i+1 < n else '\n'
        json += '\t]\n'
        json += '}\n'
        return json

    def data_iter(self):
        for channel in self.channels:
            for d in channel.data_iter():
                yield d

    def export(self, path):
        if len(self.channels) == 0:
            return

        json = self.json_str()
        file_def = open(path + '.json', 'w')
        file_def.write(json)
        file_def.close()

        file_dat = open(path + '.bytes', 'wb')
        for data in self.data_iter():
            file_dat.write(data)
        file_dat.close()

################################################################################
# Modifier
################################################################################

class Modifier:
    class Blends:
        def __init__(self, attributes=[], data=[]):
            self.attributes = attributes
            self.data = data
    class Targets:
        def __init__(self, count=0, groups=[], data=[]):
            self.count = count
            self.groups = groups
            self.data = data

    def __init__(self, name, type, blends, targets):
        self.name = name
        self.type = type
        self.blends = blends
        self.targets = targets

    def data_iter(self):
        for data in self.blends.data:
            yield data
        for data in self.targets.data:
            yield data

    def json_str(self):
        json = '{\n'
        # blends
        json += '\t"blends": {\n'
        # json += '\t\t"count": %d,\n' % (self.blends.count)
        json += '\t\t"attributes": [\n'
        n = len(self.blends.attributes)
        for (i, attr) in enumerate(self.blends.attributes):
            json += '\t\t\t' + attr.json_str()
            json += ',\n' if i+1<n else '\n'
        json += '\t\t]\n'
        json += '\t},\n'
        # targets
        json += '\t"targets": {\n'
        json += '\t\t"type": "%s",\n' % index_type_str(self.targets.count)
        json += '\t\t"groups": [\n'
        n = len(self.targets.groups)
        for (i, group) in enumerate(self.targets.groups):
            json += '\t\t\t' + group.json_str()
            json += ',\n' if i+1<n else '\n'
        json += '\t\t]\n'
        json += '\t}\n'
        json += '}\n'
        return json

    def export(self, path):
        json = self.json_str()
        file_def = open(path + '.json', 'w')
        file_def.write(json)
        file_def.close()

        file_dat = open(path + '.bytes', 'wb')
        for data in self.data_iter():
            file_dat.write(data)
        file_dat.close()

################################################################################
# Mesh
################################################################################

class Mesh:
    class Vertices:
        def __init__(self, count=0, attributes=[], data=[]):
            self.count = count
            self.attributes = attributes
            self.data = data

    class Indices:
        def __init__(self, type_='byte', groups=[], data=[]):
            self.type = type_
            self.groups = groups
            self.data = data

    class Properties:
        def __init__(self, attributes=[], data=[], bones=[], states=[]):
            self.attributes = attributes
            self.data = data
            self.bones = bones
            self.states = states

    def __init__(self, name="", vertices=Vertices(), indices=Indices(), properties=Properties()):
        self.name = name
        self.vertices = vertices
        self.indices = indices
        self.properties = properties

    def json_str(self):
        json = '{\n'
        # vertices
        json += '\t"vertices": {\n'
        json += '\t\t"count": %d,\n' % (self.vertices.count)
        json += '\t\t"attributes": [\n'
        n = len(self.vertices.attributes)
        for (i, attr) in enumerate(self.vertices.attributes):
            json += '\t\t\t' + attr.json_str()
            json += ',\n' if i+1 < n else '\n'
        json += '\t\t]\n'
        json += '\t},\n'
        # indices
        json += '\t"indices": {\n'
        json += '\t\t"type": "%s",\n' % (self.indices.type)
        json += '\t\t"topology": "%s",\n' % ("triangles")
        json += '\t\t"groups": [\n'
        n = len(self.indices.groups)
        for (i, group) in enumerate(self.indices.groups):
            json += '\t\t\t' + group.json_str()
            json += ',\n' if i+1 < n else '\n'
        json += '\t\t]\n'

        #properties
        num_attrs = len(self.properties.attributes)
        num_bones = len(self.properties.bones)
        num_states = len(self.properties.states)
        if num_attrs + num_bones + num_states > 0:
            json += '\t},\n'
            json += '\t"properties": {\n'
            if num_attrs > 0:
                json += '\t\t"attributes": [\n'
                for (i, attr) in enumerate(self.properties.attributes):
                    json += '\t\t\t' + attr.json_str()
                    json += ',\n' if i+1 < num_attrs else '\n'
                json += '\t\t]'
                json += ',\n' if num_bones + num_states > 0 else '\n'
            if num_bones > 0:
                json += '\t\t"bones": ['
                for (i, bone) in enumerate(self.properties.bones):
                    json += ' "' + bone + '"'
                    json += ',' if i+1 < num_bones else ''
                json += ']'
                json += ',\n' if num_states > 0 else '\n'
            if num_states > 0:
                json += '\t\t"states": ['
                for (i, state) in enumerate(self.properties.states):
                    json += ' "' + state + '"'
                    json += ',' if i+1 < num_states else ''
                json += ']\n'

        json += '\t}\n'
        json += '}'
        return json

    def data_iter(self):
        for data in self.vertices.data:
            yield data
        for data in self.indices.data:
            yield data
        for data in self.properties.data:
            yield data

    def export(self, path):
        json = self.json_str()
        file_def = open(path + '.json', 'w')
        file_def.write(json)
        file_def.close()

        file_dat = open(path + '.bytes', 'wb')
        for data in self.data_iter():
            file_dat.write(data)
        file_dat.close()

################################################################################
# MeshInfo
################################################################################

class MeshInfo:
    class VertexSplit:
        @staticmethod
        def find(array, map, split):
            for i in map:
                elem = array[i]
                if elem == split:
                    return i
            return -1
        def __init__(self, vertex, loop, normal=mathutils.Vector((0,0,0)), uv_layers=[], color_layers=[]):
            self.vertex = vertex
            self.loop = loop
            self.normal = normal
            self.uv_layers = uv_layers
            self.color_layers = color_layers
        def __eq__(self, other):
            if not isinstance(other, self.__class__):
                return False
            if self.vertex != other.vertex:
                return False
            if not is_equal3(self.normal, other.normal):
                return False
            if len(self.uv_layers) != len(other.uv_layers):
                return False
            for i in range(len(self.uv_layers)):
                if not is_equal2(self.uv_layers[i], other.uv_layers[i]):
                    return False
            if len(self.color_layers) != len(other.color_layers):
                return False
            for i in range(len(self.color_layers)):
                if not is_equal3(self.color_layers[i], other.color_layers[i]):
                    return False
            return True
        def __ne__(self, other):
            return not self.__eq__(other)

    class Morph:
        class Target:
            def __init__(self, name="", positions=[], normals=[]):
                self.name = name
                self.positions = positions
                self.normals = normals

        def __init__(self, name="", targets=[]):
            self.name = name
            self.targets = targets

        def create_modifier(self):
            num_frames = 0
            num_vertices = len(self.targets[0].positions) if len(self.targets) else 0
            target_groups = []
            for target in self.targets:
                indices = []
                target_groups.append(indices)
                for i in range(num_vertices):
                    if is_zero3(target.positions[i]) and is_zero3(target.normals[i]):
                        continue
                    indices.append(i)
                    num_frames += 1

            index_type = index_type_struct(num_vertices)
            s_float3 = struct.Struct('<fff')

            attributes = [
                Attribute('position', 'float', 3, num_frames),
                Attribute('normal', 'float', 3, num_frames)
            ]
            data = [ attributes[0].create_array(), attributes[1].create_array() ]

            n = 0
            for (i, target_group) in enumerate(target_groups):
                for vertex in target_group:
                    s_float3.pack_into(data[0], s_float3.size * n, *self.targets[i].positions[vertex])
                    s_float3.pack_into(data[1], s_float3.size * n, *self.targets[i].normals[vertex])
                    n += 1

            blends = Modifier.Blends(attributes, data)

            groups = [ None ] * len(target_groups)
            data = [ None ] * len(target_groups)
            for (i, target_group) in enumerate(target_groups):
                n = len(target_group)
                groups[i] = Group(self.targets[i].name, n)
                data[i] = array.array(index_type, target_group)

            targets = Modifier.Targets(num_vertices, groups, data)

            return Modifier(self.name, 'morph', blends, targets)

    class Skin:
        class Bone:
            def __init__(self, name="", pose=mathutils.Matrix.Identity(4)):
                self.name = name
                self.pose = pose
        class Influence:
            def __init__(self, index=0, weight=0.0):
                self.index = index
                self.weight = weight

        def __init__(self, name="", bones=[], weights=[], bind=True):
            self.name = name
            self.bones = bones
            self.weights = weights
            self.bind = bind

        def create_modifier(self):

            max_index = 0
            count = 0
            num_bones = len(self.bones)

            bones = [ [] for _ in range(num_bones) ]
            for (v, influences) in enumerate(self.weights):
                max_index = max(max_index, v)
                for inf in influences:
                    bones[inf.index].append( ( inf.weight, v ) )
                    count += 1
                if len(influences) == 0:
                    print ('warning: mesh[%s] vertex[%d] has no weights!' % (self.name, v))

            attr_weight = Attribute('weight', 'float', 1, count)
            data_weight = attr_weight.create_array()

            attr_bind = Attribute('bind', 'float', 16, num_bones)
            data_bind = attr_bind.create_array()

            s_index = struct.Struct('<' + (index_type_struct(max_index)) )
            s_float = struct.Struct('<f')

            groups = [ None ] * num_bones
            data = [ None ] * num_bones

            n = 0
            for (i, bone) in enumerate(bones):
                groups[i] = Group(self.bones[i].name, len(bone))
                indices = data[i] = array.array(index_type_struct(max_index), [0] * len(bone))

                for j in range(16):
                    s_float.pack_into(data_bind, (s_float.size * (16 * i + j)),  self.bones[i].pose[j//4][j%4])

                for (j, inf) in enumerate(bone):
                    s_index.pack_into(indices, s_index.size * j, inf[1])
                    s_float.pack_into(data_weight, s_float.size * n, inf[0])
                    n += 1

            if self.bind:
                blends = Modifier.Blends([ attr_weight, attr_bind ], [ data_weight, data_bind ])
            else:
                blends = Modifier.Blends([ attr_weight ], [ data_weight ])
            targets = Modifier.Targets(max_index, groups, data)

            return Modifier(self.name, 'skin', blends, targets)

    @staticmethod
    def from_bpy_object(obj):
        if obj.type != 'MESH':
            return None

        mesh = obj.data
        # print(mesh.name)

        num_mats = max(1, len(mesh.materials))
        num_verts = len(mesh.vertices)

        splits = [None] * num_verts
        groups = [[] for _ in range(num_mats)]
        split_map = [[] for _ in range(num_verts)]

        for f_i, face in enumerate(mesh.polygons):
            if len(face.vertices) < 3:
                print('warning: mesh[%s] face[%d] has less than 3 vertices -> skipping' % (mesh.name, f_i))
                continue

            face_vertices = []

            for (v_i, vertex) in enumerate(face.vertices):
                loop = face.loop_indices[v_i]
                normal = mesh.loops[loop].normal.copy()
                uvs = []
                for uv_layer in mesh.uv_layers:
                    uv = uv_layer.data[loop].uv
                    uvs.append(mathutils.Vector((uv[0], uv[1])))
                colors = []
                for color_layer in mesh.vertex_colors:
                    color = color_layer.data[loop].color
                    colors.append(mathutils.Vector((color[0], color[1], color[2])))
                split = MeshInfo.VertexSplit(vertex, loop, normal, uvs, colors)

                if not splits[vertex]:
                    splits[vertex] = split
                    split_map[vertex].append(vertex)
                    face_vertices.append(vertex)
                    continue

                split_vertex = MeshInfo.VertexSplit.find(splits, split_map[vertex], split)
                if split_vertex >= 0:
                    face_vertices.append( split_vertex )
                    continue

                split_vertex = len(splits)
                split_map[vertex].append(split_vertex)
                splits.append(split)
                face_vertices.append( split_vertex )


            points = []
            for vertex in face_vertices:
                points.append(mesh.vertices[splits[vertex].vertex].co.copy())
            if (len(points) > 4):
                map = poly_triangulate(points)
            else:
                map = poly_triangulate_concave(points)
            tri_vertices = []
            for i in map:
                tri_vertices.append(face_vertices[i])

            groups[face.material_index % num_mats].extend(tri_vertices)


        for i in range(0, num_verts):
            if not splits[i]:
                splits[i] = MeshInfo.VertexSplit(i, -1)
                print('warning: mesh[%s] has an isolated vertex[%d]' % (mesh.name, i))

        return MeshInfo(obj, splits, groups)

    def __init__(self, obj, splits=[], groups=[]):
        self.obj =  obj
        self.splits = splits
        self.groups = groups
        self.add_positions_and_normals()
        self.morph = None
        self.skin = None

    def add_positions_and_normals(self):
        mesh, splits = self.obj.data, self.splits

        positions = [ None ] * len(splits)
        normals   = [ None ] * len(splits)
        for (i, split) in enumerate(splits):
            positions[i] = mesh.vertices[split.vertex].co.copy()
            normals[i]   = mesh.loops[split.loop].normal.copy() if split.loop >= 0 else mathutils.Vector((0,0,0))

        self.positions = positions
        self.normals = normals

    def add_morph_targets(self):
        if not self.obj.data.shape_keys:
            self.morph_targets = None
            return

        morph_targets = []

        mesh = self.obj.data
        splits = self.splits

        ref = mesh.shape_keys.reference_key
        rel = mesh.shape_keys.use_relative

        ref_normals = floats_to_vec3s(ref.normals_split_get())

        for (i, block) in enumerate(mesh.shape_keys.key_blocks):
            if block == mesh.shape_keys.reference_key:
                continue

            normals = floats_to_vec3s(block.normals_split_get())

            block_positions = [ mathutils.Vector((0,0,0)) ] * len(splits)
            block_normals   = [ mathutils.Vector((0,0,0)) ] * len(splits)

            for (i, split) in enumerate(splits):
                block_positions[i] = block.data[split.vertex].co.copy()
                block_normals[i]   = normals[split.loop].copy() if split.loop >= 0 else mathutils.Vector((0,0,0))
                if rel:
                    ref_position = ref.data[split.vertex].co.copy()
                    ref_normal = ref_normals[split.loop].copy() if split.loop >= 0 else mathutils.Vector((0,0,0))
                    block_positions[i] = block_positions[i] - ref_position
                    block_normals[i] = block_normals[i] - ref_normal

            morph_targets.append(MeshInfo.Morph.Target(block.name, block_positions, block_normals))

        # clean up
        self.morph = MeshInfo.Morph(mesh.name, morph_targets)

    def add_skin(self, bind_pose=True, limit=4):
        self.skin = None

        obj = self.obj
        armature = obj.find_armature()

        if not armature or armature != obj.parent:# or obj.parent_type != 'ARMATURE':
            return
        if len(armature.pose.bones) == 0 or len(obj.vertex_groups) == 0:
            return

        num_vertex_groups = len(obj.vertex_groups)

        mesh = self.obj.data
        num_verts = len(mesh.vertices)

        bone_map = {}
        if True: # full armature
            for (bone_index, bone) in enumerate(armature.data.bones):
                bone_map[bone.name] = bone_index

        weights = [ None ] * num_verts
        for (i, v) in enumerate(mesh.vertices):
            v_weights = weights[i] = []

            for vg in v.groups:
                if vg.group < 0 or vg.group >= num_vertex_groups:
                    continue

                group = obj.vertex_groups[vg.group]
                if not group:
                    continue

                bone_index = armature.data.bones.find(group.name)
                if bone_index < 0:
                    continue # probably a blend shape
                bone = armature.data.bones[bone_index]

                if vg.weight <= 0.0:
                    print ('warning: mesh[%s] vertex[%d] bone[%s] weight is zero -> skipping' % (mesh.name, i, bone.name))
                    continue

                bone_index = bone_map.get(bone.name, -1)
                if bone_index < 0:
                    bone_map[bone.name] = bone_index = len(bone_map)

                v_weights.append(MeshInfo.Skin.Influence(bone_index, vg.weight))

            v_weights.sort(reverse=True, key=lambda i: i.weight)

            if limit > 0:
                v_weights = v_weights[0:min(limit, len(v_weights))]

            sum = 0.0
            for w in v_weights:
                sum += w.weight
            if sum < 0.99:
                print ('warning: mesh[%s] vertex[%d] weights are not normalized' % (mesh.name, i))
            for w in v_weights:
                w.weight /= sum

        for i in range(len(weights), len(self.splits)):
            weights.append(weights[self.splits[i].vertex])

        bones = [ None ] * len(bone_map)
        for (name, index) in bone_map.items():
            bone = armature.data.bones[name]
            matrix = (bone.matrix_local).inverted() @ armature.matrix_world.inverted() @ obj.matrix_world
            bones[index] = MeshInfo.Skin.Bone(armature.name + "." + name, matrix)

        self.skin = MeshInfo.Skin(mesh.name, bones, weights, bind_pose)

    def create_mesh(self, bake_morph_targets=True, bake_skin=True):
        mesh = self.obj.data
        positions = self.positions
        normals = self.normals
        splits = self.splits
        face_groups = self.groups

        num_verts = len(splits)
        num_colors = len(mesh.vertex_colors)
        num_uvs = len(mesh.uv_layers)
        num_mats = len(mesh.materials)
        num_morphs = 0
        num_weights = 0
        num_bones = 0

        frames = 1
        if bake_morph_targets and self.morph:
            num_morphs = len(self.morph.targets)
            frames += num_morphs

        if bake_skin and self.skin:
            for w in self.skin.weights:
                num_weights = max(num_weights, len(w))
            num_bones = len(self.skin.bones)

        attributes = [
            Attribute('position', 'float', 3, frames),
            Attribute('normal', 'float', 3, frames),
        ]
        if num_uvs > 0:
            attributes.append(Attribute('texcoord', 'float', 2, num_uvs))
        if num_colors > 0:
            attributes.append(Attribute('color', 'float', 3, num_colors))
        if num_bones and num_weights:
            attributes.append(Attribute('weight', 'float', num_weights))
            attributes.append(Attribute('bone', index_type_str(num_bones), num_weights))


        data = [ None ] * len(attributes)
        for (i, attr) in enumerate(attributes):
            data[i] = attr.create_array(num_verts)

        s_index = struct.Struct('<' + (index_type_struct(num_bones)) )
        s_float = struct.Struct('<f')
        s_float2 = struct.Struct('<ff')
        s_float3 = struct.Struct('<fff')

        for (i, split) in enumerate(splits):
            position = positions[i]
            normal = normals[i]
            s_float3.pack_into(data[0], s_float3.size*i, *position)
            s_float3.pack_into(data[1], s_float3.size*i, *normal)
            # morph targets
            for frame in range(0, num_morphs):
                morph_target = self.morph.targets[frame]
                p = morph_target.positions[i] + position
                n = morph_target.normals[i] + normal
                s_float3.pack_into(data[0], s_float3.size*((frame+1)*num_verts + i), *p)
                s_float3.pack_into(data[1], s_float3.size*((frame+1)*num_verts + i), *n)

            # uvs
            off = 2
            for (j, uv) in enumerate(split.uv_layers):
                s_float2.pack_into(data[off], s_float2.size * (j*num_verts+i), *uv)

            # colors
            off += num_uvs
            for (j, color) in enumerate(split.color_layers):
                s_float3.pack_into(data[off], s_float3.size * (j*num_verts + i), *color)

            # weights
            off += num_colors
            if num_bones and num_weights:
                skin_weights = self.skin.weights[i]
                for (j, skin_weight) in enumerate(skin_weights):
                    s_float.pack_into(data[off+0], s_float.size * (num_weights * i + j), skin_weight.weight)
                    s_index.pack_into(data[off+1], s_index.size * (num_weights * i + j), skin_weight.index)


        vertices = Mesh.Vertices(num_verts, attributes, data)

        groups = [ None ] * len(face_groups)
        data = [ None ] * len(face_groups)


        index_type = index_type_struct(num_verts)
        s_index = struct.Struct('<' + index_type)


        for (i, face_group) in enumerate(face_groups):
            count = len(face_group)
            mat = mesh.materials[i % num_mats] if num_mats else None
            name = mat.name if mat else ''
            groups[i] = Group(name, count)
            data[i] = array.array(index_type, face_group)

        indices = Mesh.Indices(index_type_str(num_verts), groups, data)

        attributes = []
        data = []
        bones = []
        states = []

        if num_bones and num_weights:
            for (i, bone) in enumerate(self.skin.bones):
                bones.append(bone.name)
            if self.skin.bind:
                attr = Attribute("bind", "float", 16, num_bones)
                buf = attr.create_array()
                attributes.append(attr)
                data.append(buf)
                for (i, bone) in enumerate(self.skin.bones):
                    for j in range(0, 16):
                        s_float.pack_into(buf, (s_float.size * (16 * i + j)),  bone.pose[j//4][j%4])

        if bake_morph_targets and self.morph:
            for target in self.morph.targets:
                states.append(target.name)

        properties = Mesh.Properties(attributes, data, bones, states)

        return Mesh(mesh.name, vertices, indices, properties)

################################################################################
# Transform
################################################################################

class Transform:
    @staticmethod
    def from_matrix(m):
        t, r, s = m.decompose()
        r.normalize()
        return Transform(t, r, s)

    def __init__(self, t=[0.0,0.0,0.0], r=[0.0,0.0,0.0], s=[1.0,1.0,1.0]):
        self.translate = t
        self.rotate = r
        self.scale = s

    def json_str(self):
        json = '{'
        json += ' "translate": [ %f, %f, %f ]' % (self.translate.x, self.translate.y, self.translate.z)
        if not is_zero3((self.rotate.x, self.rotate.y, self.rotate.z)):
            json += ', "rotate": [ %f, %f, %f ]' % (self.rotate.x, self.rotate.y, self.rotate.z)
        if not is_one3(self.scale):
            json += ', "scale": [ %f, %f, %f ]' % (self.scale.x, self.scale.y, self.scale.z)
        json += ' }'
        return json

################################################################################
# Model
################################################################################

class Model:
    class Shape:
        def __init__(self, name="", bone=0):
            self.name = name
            self.bone = bone
        def json_str(self):
            json = '{ "name": "%s"' % (self.name)
            if self.bone > 0:
                json += ', "bone": %d' % (self.bone)
            json += ' }'
            return json

    class State:
        @staticmethod
        def from_bpy_object(obj):
            if not obj.data or not obj.data.shape_keys:
                return []
            states = []
            ref = obj.data.shape_keys.reference_key
            for block in obj.data.shape_keys.key_blocks:
                if block.name == ref.name:
                    continue
                states.append(Model.State( obj.name + "." + block.name, block.value))
            return states
        def __init__(self, name="", value=0.0):
            self.name = name
            self.value = value
        def json_str(self):
            return '{ "name": "%s", "value": %f }' % (self.name, self.value)

    class Bone:
        def __init__(self, name="", transform=Transform(), parent=0):
            self.name = name
            self.transform = transform
            self.parent = parent
        def json_str(self):
            json = '{ "name": "%s", ' % (self.name)
            if self.parent > 0:
                json += '"parent": %d, ' % (self.parent)
            json += '"transform": %s }' % (self.transform.json_str())
            return json

    @staticmethod
    def from_bpy_object(obj):
        if obj.type == "EMPTY":
            return Model.from_bpy_object_empty(obj)
        if obj.type == "ARMATURE":
            return Model.from_bpy_object_armature(obj)
        # if obj.type == 'MESH':
        #     return from_bpy_object_mesh(obj)

    @staticmethod
    def from_bpy_object_empty(obj):
        states = []
        bones = []
        shapes = []
        for child in obj.children:
            if child.hide_viewport or not is_empty_mesh(child):
              continue
            # state
            states.extend(Model.State.from_bpy_object(child))
            # bone
            matrix = (child.parent.matrix_world).inverted() @ child.matrix_world
            transform = Transform.from_matrix(matrix)
            bones.append(Model.Bone(child.name, transform))
            # shape
            if child.data:
                name = child.name
                if name != child.data.name:
                    name += "@" + child.data.name
                shapes.append(Model.Shape(name, len(bones)))

        return Model(obj.name, shapes, bones, states)

    @staticmethod
    def from_bpy_object_armature(obj):
        states = []
        bones = []
        shapes = []

        for bone in obj.data.bones:
            # bone
            matrix = bone.matrix_local
            if bone.parent != None:
                matrix = (bone.parent.matrix_local).inverted() @ matrix
            transform = Transform.from_matrix(matrix)
            parent = obj.data.bones.find(bone.parent.name) + 1 if bone.parent else 0
            name = obj.name + "." + bone.name
            bones.append(Model.Bone(name, transform, parent))

        for child in obj.children:
            if child.hide_viewport:
                continue
            if is_armature_mesh(child):
                # state
                states.extend(Model.State.from_bpy_object(child))
                # shape
                shapes.append(Model.Shape(child.name))
            elif is_bone_mesh(child):
                # state
                states.extend(Model.State.from_bpy_object(child))
                # bone
                bone = obj.data.bones[child.parent_bone]
                parent = obj.data.bones.find(bone.name) + 1
                # matrix = (child.parent.matrix_world @ bone.matrix).inverted() @ child.matrix_world
                matrix = (child.parent.matrix_world @ bone.matrix_local).inverted() @ child.matrix_world
                transform = Transform.from_matrix(matrix)
                bones.append(Model.Bone(child.name, transform, parent))
                # shape
                if child.data:
                    name = child.name
                    if child.data and name != child.data.name:
                        name += "@" + child.data.name
                    shapes.append(Model.Shape(name, len(bones)))

        return Model(obj.name, shapes, bones, states)


    def __init__(self, name="", shapes=[], bones=[], states=[]):
        self.name = name
        self.shapes = shapes
        self.bones = bones
        self.states = states

    def json_str(self):
        json = '{\n'

        # bones
        n = len(self.bones)
        if n > 0:
            json += '\t"bones": [\n'
            for (i, bone) in enumerate(self.bones):
                json += '\t\t%s' % (bone.json_str())
                json += ',\n' if i+1 < n else '\n'
            json += '\t]'
            json += ',\n' if len(self.states) + len(self.shapes) > 0 else '\n'
        # states
        n = len(self.states)
        if n > 0:
            json += '\t"states": [\n'
            for (i, state) in enumerate(self.states):
                json += '\t\t%s' % (state.json_str())
                json += ',\n' if i+1 < n else '\n'
            json += '\t]'
            json += ',\n' if len(self.shapes) > 0 else '\n'
        # shapes
        n = len(self.shapes)
        if n > 0:
            json += '\t"shapes": [\n'
            for (i, shape) in enumerate(self.shapes):
                json += '\t\t%s' % (shape.json_str())
                json += ',\n' if i+1 < n else '\n'
            json += '\t]'
            json += '\n'

        json += '}\n'
        return json

    def export(self, path):
        json = self.json_str()
        file_def = open(path + '.json', 'w')
        file_def.write(json)
        file_def.close()

################################################################################
# Scene
################################################################################

class Scene:
    class Object:
        @staticmethod
        def from_bpy_object(obj):
            name = obj_name(obj)
            transform = Transform.from_matrix(obj.matrix_local)
            return Scene.Object(name, transform)

        def __init__(self, name="", transform=Transform()):
            self.name = name
            self.transform = transform

        def json_str(self):
            return '{ "name": "%s", "transform": %s }' % (self.name, self.transform.json_str())

    @staticmethod
    def from_bpy_scene(scene):
        name = scene.name
        objects = findRootObjects(scene.objects)
        for (i, obj) in enumerate(objects):
            objects[i] = Scene.Object.from_bpy_object(obj)
        return Scene(name, objects)

    def __init__(self, name="", objects=[]):
        self.name = name
        self.objects = objects

    def json_str(self):
        n = len(self.objects)
        json = '{\n'
        json += '\t"objects": [\n'
        for (i, obj) in enumerate(self.objects):
            json += '\t\t%s' % (obj.json_str())
            json += ',\n' if i+1 < n else '\n'
        json += '\t]\n'
        json += '}\n'
        return json

    def export(self, path):
        json = self.json_str()
        file_def = open(path + '.json', 'w')
        file_def.write(json)
        file_def.close()

################################################################################
# Exporter
################################################################################

def read_file_bytes(path):
    with open(path,'rb') as file:
        return file.read()

use_fixed_time = True
time_zero = (1980, 3, 7, 0, 0, 0)

##########
#export class registration and interface
from bpy.props import *
from bpy_extras.io_utils import (ExportHelper, path_reference_mode, axis_conversion)


class ExportMosaic(bpy.types.Operator):#, ExportHelper):
  '''Export to Mosaic formats'''
  bl_idname = "export.mosaic"
  bl_label = 'Mosaic Export'

  clean = False
  filepath: StringProperty(subtype='DIR_PATH',name="Directory Path", description="Directory for exporting", maxlen=1024, default=os.path.expanduser('~'))

  export_meshes: BoolProperty(name="Meshes", description="Export Meshes",default=True)
  export_mesh_morph: BoolProperty(name="Morph Targets", description="Export Shape Keys",default=True)
  export_mesh_morph_baked: BoolProperty(name="Bake", description="Bake Morph-Targets into mesh",default=False)
  export_mesh_skin: BoolProperty(name="Skin", description="Export Armature Deformation",default=True)
  export_mesh_skin_baked: BoolProperty(name="Bake", description="Bake Skin into mesh",default=False)
  export_mesh_skin_bind: BoolProperty(name="Bind", description="Export bind pose",default=True)

  export_models: BoolProperty(name="Models", description="Export Models",default=True)
  export_model_anim: BoolProperty(name="Animations", description="Include Model Animations",default=True)
  export_model_anim_nla: BoolProperty(name="NLA Actions", description="Slice animation by NLA track ranges",default=True)

  export_scene: BoolProperty(name="Scene", description="Export Scene",default=True)
  export_scene_anim: BoolProperty(name="Animations", description="Include Scene Animations",default=True)

  zipModes =  [ ('None', 'None', 'Not Zipped'), ('Types', 'Types', 'Zip each type separately'), ('All', 'All', 'Zip all types together') ]
  export_zip_mode: EnumProperty(name="Zip", description="Zip Assets", items=zipModes, default='Types')
  export_zip_compress: BoolProperty(name="Compress", description="Compress zipped files", default=True)

  files = {}
  textures = []

  def draw(self, context):
    layout = self.layout


    row = layout.row()
    row.prop(self, 'export_meshes')
    row.prop(self, 'export_models')
    row.prop(self, 'export_scene')

    if self.properties.export_meshes:
        col = layout.box().column()
        col.label(text='Mesh Options', icon='PLUS')
        col.prop(self, 'export_mesh_morph')
        # if self.properties.export_mesh_morph:
            # row = col.row()
            # row.alignment = 'RIGHT'
            # row.prop(self, 'export_mesh_morph_baked')

        col.prop(self, 'export_mesh_skin')
        if self.properties.export_mesh_skin:
            row = col.row()
            row.alignment = 'RIGHT'
            row.prop(self, 'export_mesh_skin_bind')
            # row.prop(self, 'export_mesh_skin_baked')


    if self.properties.export_models:
        col = layout.box().column()
        col.label(text='Model Options', icon='PLUS')
        col.prop(self, 'export_model_anim')
        if self.properties.export_model_anim:
            row = col.row()
            row.alignment = 'RIGHT'
            row.prop(self, 'export_model_anim_nla')

    if self.properties.export_scene:
        col = layout.box().column()
        col.label(text='Scene Options', icon='PLUS')
        col.prop(self, 'export_scene_anim')

    row = layout.row()
    row.prop(self, 'export_zip_mode')
    if self.export_zip_mode != 'None':
        row.prop(self, 'export_zip_compress')


  def parse_command_line_options(self):
    myArgs = []
    argsStartPos = 0

    if (("--" in sys.argv) == False):
      return

    argsStartPos = sys.argv.index("--")
    argsStartPos += 1
    myArgs = sys.argv[argsStartPos:]

    try:
      opts, args = getopt.getopt(myArgs, 'hm:c', ["help", "model-file=", "clean", "skip-scene"])
    except getopt.GetoptError:
      print("Opt Error.")
      return

    for opt, arg in opts:
      if (opt in ("-h", "--help")):
        print("Run this as the following blender command.")
        print("\tblender <blend file> --background --python <script file> -- -m <output file> -c --skip-scene")
      elif (opt in ("-m", "--model-file")):
        self.filepath = arg
      elif (opt in ("--skip-scene")):
        self.export_scene = False
      elif (opt in ("-c", "--clean")):
        self.clean = True

  def invoke(self, context, event):
    WindowManager = context.window_manager
    WindowManager.fileselect_add(self) # alternatively: WindowManager.add_fileselect(self)
    return {"RUNNING_MODAL"}

  def execute(self, context):
    self.files = {}
    self.textures = []

    self.parse_command_line_options()

    if self.clean:
        context.scene.frame_set(context.scene.frame_start)
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')

    if not self.filepath.endswith(os.sep):
        self.filepath += os.sep

    if not os.path.isdir(self.filepath):
        os.makedirs(self.filepath)


    if self.export_meshes:
        self.save_textures(context)
        self.save_meshes(context)
    if self.export_models:
        self.save_models(context)
    if self.export_scene:
        self.save_scene(context)

    compression = zipfile.ZIP_DEFLATED if self.export_zip_compress else zipfile.ZIP_STORED

    if self.export_zip_mode == 'Types':
        for obj, files in self.files.items():
            zip = zipfile.ZipFile(self.filepath + obj, mode='w')
            try:
                for file in files:
                    name = file[0]
                    path = file[1]
                    if os.path.isfile(path):
                        if use_fixed_time:
                            bytes = read_file_bytes(path)
                            info = zipfile.ZipInfo(name, time_zero)
                            zip.writestr(info,bytes, compression)
                        else:
                            zip.write(path, name, compression)
                        os.remove(path)
            finally:
                zip.close()
    elif self.export_zip_mode == 'All':
        zip = zipfile.ZipFile(self.filepath + context.scene.name + '.mosaic', mode='w')
        try:
            for obj, files in self.files.items():
                for file in files:
                    path = file[1]
                    name = os.path.basename(path)
                    if os.path.isfile(path):
                        if use_fixed_time:
                            bytes = read_file_bytes(path)
                            info = zipfile.ZipInfo(name, time_zero)
                            zip.writestr(info,bytes, compression)
                        else:
                            zip.write(path, name, compression)
                        os.remove(path)
            for file in self.textures:
                path = file[1]
                if not os.path.isfile(path):
                    continue
                name = os.path.basename(path)
                if use_fixed_time:
                    bytes = read_file_bytes(path)
                    info = zipfile.ZipInfo(name, time_zero)
                    zip.writestr(info, bytes, compression)
                else:
                    zip.write(path, name, compression)
                os.remove(path)

        finally:
            zip.close()

    return {'FINISHED'}

  def save_textures(self, context):
    textures = []

    scene = context.scene
    for obj in findMeshes(scene.objects):
        if obj.type != 'MESH':
            continue

        for mat_slot in obj.material_slots:
            textures += find_material_textures(mat_slot.material)


    copy_set = set()
    for texture in textures:
        bpy_extras.io_utils.path_reference(texture,
                                            os.path.dirname(bpy.data.filepath),
                                            self.filepath,
                                            mode = 'COPY',
                                            copy_subdir = "",
                                            copy_set = copy_set)

    self.textures = [cp for cp in copy_set if os.path.isfile(cp[0]) ]
    # self.textures = list(copy_set)
    bpy_extras.io_utils.path_reference_copy(self.textures)

  def save_meshes(self, context):
    scene = context.scene
    for obj in findMeshes(scene.objects):
        if obj.type != 'MESH':
            continue

        # prepare

        mesh = obj.data
        name = mesh.name

        gen_normals = not mesh.has_custom_normals
        if gen_normals:
            mesh.create_normals_split()
        mesh.calc_normals_split()

        mesh_info = MeshInfo.from_bpy_object(obj)
        if self.export_mesh_morph:
            mesh_info.add_morph_targets()
        if self.export_mesh_skin:
            mesh_info.add_skin(self.export_mesh_skin_bind)

        if gen_normals:
            mesh.free_normals_split()

        # export

        files = [ ]

        mesh = mesh_info.create_mesh(self.export_mesh_morph_baked, self.export_mesh_skin_baked)
        mesh.export(self.filepath + name + '.mesh')
        files.extend([('mesh.json', self.filepath + name + '.mesh.json'),
                      ('mesh.bytes', self.filepath + name + '.mesh.bytes')])

        if mesh_info.morph and not self.export_mesh_morph_baked:
            morph = mesh_info.morph.create_modifier()
            morph.export(self.filepath + name + '.mesh.morph')
            files.extend([('morph.json', self.filepath + name + '.mesh.morph.json'),
                          ('morph.bytes', self.filepath + name + '.mesh.morph.bytes')])
        if mesh_info.skin and not self.export_mesh_skin_baked:
            skin = mesh_info.skin.create_modifier()
            skin.export(self.filepath + name + '.mesh.skin')
            files.extend([('skin.json', self.filepath + name + '.mesh.skin.json'),
                          ('skin.bytes', self.filepath + name + '.mesh.skin.bytes')])

        self.files[name + '.mosaic-mesh'] = files

  def save_models(self, context):
    scene = context.scene
    for obj in findModels(scene.objects):
        model = Model.from_bpy_object(obj)
        model.export(self.filepath + obj.name + '.model')
        self.files[obj.name + '.mosaic-model'] = [ ('model.json', self.filepath + obj.name + '.model.json')]

    if not self.export_model_anim:
        return

    for obj in findModels(scene.objects, True):
        anim = Animation.for_bpy_object(scene, obj, self.export_model_anim_nla)
        anim.export(self.filepath + obj.name + '.model.anim')
        self.files[obj.name + '.mosaic-model'].extend([('anim.json', self.filepath + obj.name + '.model.anim.json'),
                                                ('anim.bytes', self.filepath + obj.name + '.model.anim.bytes')])

  def save_scene(self, context):
    scene = context.scene
    scn = Scene.from_bpy_scene(scene)
    scn.export(self.filepath + scn.name + '.scene')
    self.files[scn.name + '.mosaic-scene'] = [ ('scene.json', self.filepath + scn.name + '.scene.json')]

    if not self.export_scene_anim:
        return

    anim = Animation.from_bpy_scene(scene)
    anim.export(self.filepath + scn.name + '.scene.anim')
    self.files[scn.name + '.mosaic-scene'].extend([('anim.json', self.filepath + scn.name + '.scene.anim.json'),
                                            ('anim.bytes', self.filepath + scn.name + '.scene.anim.bytes')])


