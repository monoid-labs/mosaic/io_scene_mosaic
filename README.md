# Mosaic Module for Blender3D

Exports mosaic assets from [Blender](https://www.blender.org/).

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))
